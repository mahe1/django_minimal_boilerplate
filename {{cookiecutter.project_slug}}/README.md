# Minimal Django 2.1 Boilerplate #

Following are the Prerequisites for installing Cookiecutter:

    pip
    virtualenvwrapper
    python 3.6
    PostgreSQL

Create a virtualenv for your project and activate it:
```
$ mkvirtualenv -p python3.6 <virtualenv name>
$ pip install cookiecutter
```
Change directories into the folder where you want your project to live. Now execute the following command to generate a django project:
```
$ cookiecutter https://mahe1@bitbucket.org/mahe1/mariocutter.git
```
This command runs cookiecutter with the mariocutter repo, asking us to enter project-specific details.
```
project_slug []: 
author_name []:
email []:
domain_name []:
```
```
$ pip install -r requirements/local.txt
```
Enter secret key and database specific details

secrets.json
```
{
  "secret_key" : "",
  "dbbackend" : "django.db.backends.postgresql_psycopg2",
  "dbname": "{{ cookiecutter.project_slug }}",
  "dbuser" : "",
  "dbpass" : "",
  "dbhost" : "localhost",
  "dbport" : "5432"
}
```
```
$ python manage.py migrate
$ python manage.py runserver
```

