from .base import *

DEBUG = False

ALLOWED_HOSTS = [
    '{{ cookiecutter.domain_name }}',
]

STATIC_URL = 'http://static.{{ cookiecutter.domain_name }}/assets/'
MEDIA_URL = 'http://static.{{ cookiecutter.domain_name }}/media/'



STATICFILES_DIRS = (
    ROOT + "/static",
)

# SECURITY
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#session-cookie-secure
SESSION_COOKIE_SECURE = True
# https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-secure
CSRF_COOKIE_SECURE = True
