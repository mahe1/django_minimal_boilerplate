from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = [
    "localhost",
    "0.0.0.0",
    "127.0.0.1",
]

STATIC_URL = '/static/'
MEDIA_URL = 'http://localhost:8000/media/'

STATICFILES_DIRS = [
    os.path.join(ROOT, "static"),
]

if DEBUG:
    MIDDLEWARE = MIDDLEWARE + [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]
    INSTALLED_APPS = INSTALLED_APPS + [
        'debug_toolbar',
            ]
    INTERNAL_IPS = ('127.0.0.1', )
    DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
        'SHOW_TEMPLATE_CONTEXT': True,
        'DISABLE_PANELS': [
        'debug_toolbar.panels.redirects.RedirectsPanel',
        ],
    }
