from django.conf import settings
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.static import serve
from functools import partial
from django.conf.urls.static import static
from django.views.defaults import bad_request, page_not_found, permission_denied, server_error

from . import views

admin.site.site_header = ("Mahe's Administration")
admin.site.site_title = ("Administration")

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home ),
    path('i18n/', include('django.conf.urls.i18n')),
]

urlpatterns += [
    path('400/', partial(bad_request, exception=None)),
    path('403/', partial(permission_denied, exception=None)),
    path('404/', partial(page_not_found, exception=None)),
    path('500/', server_error)
]

if settings.DEBUG:
    urlpatterns +=  [
        re_path(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT,
        }),
        re_path(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT,
        }),
    ]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]

